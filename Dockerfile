FROM opsperator/nodejs AS builder

ARG COMRADE_VERSION=master
ENV COMRADE_REPO=https://github.com/moshe/elasticsearch-comrade
COPY config/patched-pipfile /

USER 0
RUN set -x \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get update \
    && apt-get install -y git python3-pip \
    && pip3 install --upgrade pip \
    && pip3 install pipenv \
    && if ! git clone https://github.com/moshe/elasticsearch-comrade.git \
	    -b "v$COMRADE_VERSION" /usr/src/elasticsearch-comrade; then \
	if ! git clone https://github.com/moshe/elasticsearch-comrade.git \
		-b "$COMRADE_VERSION" /usr/src/elasticsearch-comrade; then \
	    echo "FAILED cloning $COMRADE_REPO:$COMRADE_VERSION"; \
	    exit 1; \
	fi; \
    fi \
    && cd /usr/src/elasticsearch-comrade \
    && mv /patched-pipfile ./Pipfile \
    && pipenv install -e . --skip-lock >/dev/null 2>&1 \
    && cd /usr/src/elasticsearch-comrade/client \
    && if ! grep nobody /etc/passwd >/dev/null; then \
	echo nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin \
	    >>/etc/passwd; \
    fi \
    && if ! grep nobody /etc/group >/dev/null; then \
	echo nobody:x:65534: >>/etc/group; \
    fi \
    && npm install --unsafe-perm -g @vue/cli-service @vue/cli-plugin-babel \
	@vue/cli-plugin-eslint vue-cli-plugin-vuetify @vue/babel-preset-app \
	"@babel/core@*" \
    && npm install @vue/babel-preset-app \
    && NODE_ENV=production npm install >/dev/null \
    && NODE_ENV=production npm run build >/dev/null \
    && mv /usr/src/elasticsearch-comrade/client /usr/src/client

FROM docker.io/python:3.7-slim-buster

# ElasticSearch Comrade image for OpenShift Origin

ARG COMRADE_VERSION=master
ARG DO_UPGRADE=
LABEL io.k8s.description="ElasticSearch Comrade Image - $COMRADE_VERSION." \
      io.k8s.display-name="ElasticSearch Comrade" \
      io.openshift.expose-services="8000:http" \
      io.openshift.tags="elasticsearch,comrade" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-escomrade" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$COMRADE_VERSION"

COPY --from=builder /usr/src/elasticsearch-comrade /app
COPY --from=builder /usr/src/client/dist /app/comrade/static
COPY config/run-comrade.sh config/patched-pipfile /

ENV DEBIAN_FRONTEND=noninteractive \
    HOME=/var/tmp

WORKDIR /app

RUN set -x \
    && rm -rf /var/lib/apt/lists/* \
    && echo "# Install Dumb-init" \
    && apt-get update \
    && apt-get install -y dumb-init \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install ElasticSearch Comrade Dependencies" \
    && apt-get install -y --no-install-recommends gcc python-dev g++ make \
	libffi-dev libssl-dev \
    && mkdir -p /app/comrade/clusters /var/tmp \
    && pip3 install --upgrade pip \
    && pip3 install pipenv \
    && mv /patched-pipfile ./Pipfile \
    && pipenv install -e . --skip-lock >/dev/null 2>&1 \
    && echo "# Fix Permissions" \
    && chown -R 1001:0 /app /var/tmp \
    && chmod -R g=u /app /var/tmp \
    && echo "# Cleaning Up" \
    && apt-get purge -y --auto-remove gcc python-dev g++ make libffi-dev \
	libssl-dev \
    && apt-get autoremove -y --purge \
    && apt-get clean \
    && rm -rf /usr/share/man /usr/share/doc /var/lib/apt/lists/* /app/.git* \
	/app/Dockerfile /app/LICENSE /app/README.md /app/docker-compose.yaml \
	/app/docs /app/e2e /app/.automerge.yml /app/docker-compose.yml \
	/app/.circleci /var/cache/* /var/tmp/.cache \
    && find /usr/local/lib/python3.7/ -type d -name __pycache__ | xargs rm -rf \
    && find /var/tmp/.local/ -type d -name __pycache__ | xargs rm -rf \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

ENTRYPOINT [ "dumb-init", "--", "/run-comrade.sh" ]
CMD "pipenv" "run" "comrade"
USER 1001
