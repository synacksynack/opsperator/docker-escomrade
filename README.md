# k8s ElasticSearch Comrade

Based on https://github.com/moshe/elasticsearch-comrade

Build with:

```
$ make build
```

Test with:

```
$ make test
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name  |    Description                  | Default                 |
| :---------------- | ------------------------------- | ----------------------- |
|  `ES_CLUSTER`     | ElasticSearch Cluster Name      | `local`                 |
|  `ES_HOST`        | ElasticSearch Remote(s)         | `127.0.0.1`             |
|  `ES_PORT`        | ElasticSearch Port              | `9200`                  |
|  `ES_PASSWORD`    | ElasticSearch Auth Password     | undef                   |
|  `ES_PROTO`       | ElasticSearch Protocol          | `http`                  |
|  `ES_USER`        | ElasticSearch Auth Username     | undef                   |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point             | Description                              |
| :------------------------------ | ---------------------------------------- |
|  `/app/comrade/clusters`        | Comrade Clusters Configuration Directory |
