SKIP_SQUASH?=1
FRONTNAME=opsperator
-include Makefile.cust

.PHONY: build
build:
	SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: test
test:
	@@if sysctl -a >/dev/null 2>&1; then \
	    if ! sysctl vm.max_map_count | grep 262144 >/dev/null; then \
		if test `id -u` = 0; then \
		    sysctl -w vm.max_map_count=262144; \
		else \
		    sudo sysctl -w vm.max_map_count=262144; \
		fi; \
	    fi; \
	else \
	    echo WARNING: if startup fails, sysctl -w vm.max_map_count=262144; \
	fi
	docker rm -f testes || echo ok
	docker rm -f testcomrade || echo ok
	docker run --name testes \
	    -p 9200:9200 \
	    -p 9300:9300 \
	    -e HOSTNAME=es-0 \
	    -d opsperator/elasticsearch
	MAINDEV=`ip r | awk '/default/{print $$0;exit;}' | sed 's|.* dev \([^ ]*\).*|\1|'`; \
	MAINIP=`ip r | awk "/ dev $$MAINDEV .* src /" | sed 's|.* src \([^ ]*\).*$$|\1|'`; \
	docker run --name testcomrade \
	     -e ES_HOST=$$MAINIP \
	    -p 8000:8000 \
	    -it opsperator/escomrade

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in service statefulset deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "ESCOMRADE_REPOSITORY_REF=$$BRANCH" \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "ESCOMRADE_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: ocpurge
ocpurge: occheck
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    | oc delete -f- || true
