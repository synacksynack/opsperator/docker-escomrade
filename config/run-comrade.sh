#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

if ! ls /app/comrade/clusters | grep -E '.json$'; then
    ES_CLUSTER=${ES_CLUSTER:-local}
    ES_HOST=${ES_HOST:-127.0.0.1}
    ES_PORT=${ES_PORT:-9200}
    ES_PROTO=${ES_PROTO:-http}
    ES_CONSTR=""
    for host in $ES_HOST
    do
	if test "$ES_CONSTR"; then
	    ES_CONSTR="$ES_CONSTR,"
	fi
	ES_CONSTR="$ES_CONSTR\"$ES_PROTO://$host:$ES_PORT\""
    done
    if test "$ES_USER" -a "$ES_PASSWORD"; then
	cat <<EOF
{
    "name": "$ES_CLUSTER",
    "params": {
	    "http_auth": ["$ES_USER", "$ES_PASSWORD"],
	    "hosts": [$ES_CONSTR]
	}
}
EOF
    else
	cat <<EOF
{
    "name": "$ES_CLUSTER",
    "params": {
	    "hosts": [$ES_CONSTR]
	}
}
EOF
    fi >/app/comrade/clusters/$ES_CLUSTER.json
fi

echo "Starting Comrade"
exec "$@"
